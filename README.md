# Fastify SSLGen

A simple Fastify plugin which provides an ability to generate development or self signed SSL certificates.

## Installation

Install the package normally using yarn.

```sh
yarn add fastify-sslgen
```

## Usage

```javascript
import fastify = require("fastify")
import sslgen from "fastify-sslgen";

const server = fastify({
    http2: true,
    https: true,
});

server.register(sslgen, {
    key: "./ssl/private/key.pem",
    cert: "./ssl/certificate.pem",
});

server.listen(8443);
```

### Available Options

- *key* - A path to read or store the private SSL key.
- *cert* - A path to read or store the certificate file.
- *info* - Provide information to use when generating the SSL certificates;
    - *altNames* - Defaults to [ "0.0.0.0", "127.0.0.1" ]
    - *commonName* - Defaults to "0.0.0.0"
    - *country* - Defaults to "AU"
    - *days* - Defaults to 30
    - *emailAddress* - Defaults to "admin@example.com"
    - *locality* - Defaults to "Somewhere"
    - *organization* - Defaults to "Localhost"
    - *organizationUnit* - Defaults to "Development"
    - *state* - Defaults to "QLD"
