export interface ISSLPluginConfig {
    /** The filename of the SSL certificate key. */
    key: string;
    /** The filename of the SSL certificate */
    cert: string;
    /** ANy information to use when generating the certificate. */
    info?: ISSLInfo;
}
export interface ISSLInfo {
    altNames?: string[];
    commonName?: string;
    country?: string;
    days?: number;
    emailAddress?: string;
    locality?: string;
    organization?: string;
    organizationUnit?: string;
    state?: string;
}
/**
 * The function to create the SSL certificate files.
 *
 * @param publicKey The filename of the public key.
 * @param privatekey The filename of the private key.
 * @param info The information to use when generating the certificate.
 *
 * @return Returns an object with the certificate and private key filenames.
 */
export declare function SSLCertificates(publicKey: string, privateKey: string, info?: ISSLInfo): Promise<{
    cert: string;
    key: string;
}>;
declare const _default: any;
export default _default;
